package main

import (
	"encoding/json"
	"fmt"
	"log"
)

func main() {
	type Movie struct {
		Title  string
		Year   int  `json:"released"`
		Color  bool `json:"color,omitempty"`
		Actors []string
	}

	var movies = []Movie{
		{Title: "Casablanca", Year: 1942, Color: false, Actors: []string{"Humphery Bogart", "Ingrid Bergman"}},
		{Title: "Cool hand Luke", Year: 1967, Color: true, Actors: []string{"Paul Newman"}},
		{Title: "Bullit", Year: 1968, Color: true, Actors: []string{"Sreve McQueen", "Jacquline Bisset"}},
	}

	data, err := json.MarshalIndent(movies, "", " ")
	if err != nil {
		log.Fatalf("marshalling JSON %s", err)
	}
	fmt.Printf("%s\n", data)

	var titles []struct{ Title string }
	if err := json.Unmarshal(data, &titles); err != nil {
		log.Fatal("JSON emarshal error # %n", err)

	}
	fmt.Println(titles)
}
